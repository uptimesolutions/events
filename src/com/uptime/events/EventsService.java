/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.events;

import com.datastax.oss.driver.api.core.uuid.Uuids;
import com.uptime.cassandra.CassandraConstants;
import com.uptime.cassandra.CassandraData;
import com.uptime.cassandra.events.dao.EventsApplicationDAO;
import com.uptime.cassandra.events.dao.EventsMapperImpl;
import com.uptime.cassandra.events.entity.Events;
import com.uptime.cassandra.events.entity.EventsApplication;
import com.uptime.events.controller.EventController;
import com.uptime.events.controller.NotificationsController;
import com.uptime.events.http.client.EmailClient;
import com.uptime.events.http.listeners.RequestListener;
import com.uptime.services.AbstractService;
import com.uptime.services.vo.ServiceHostVO;
import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 *
 * @author ksimmons
 */
public class EventsService extends AbstractService{
    public final static String SERVICE_NAME = "Events";
    public static String IP_ADDRESS = null;
    public static int PORT = 0;
    public static final Logger LOGGER = Logger.getLogger(EventsService.class.getName());
    public final static Logger CQL_LOGGER = Logger.getLogger("CQL Logger");
    public static Semaphore mutex = new Semaphore(1);
    public static boolean running = true;
    public static final String[] names = {"Email"};
    public static EmailClient CLIENT;
    private static RequestListener listener;
    private final int READ_TIMEOUT = 5000;
    private final int CONNECTION_TIMEOUT = 5000;
    private static final AtomicLong EMAIL_INDEX = new AtomicLong(0L);
    public static EventController EVENT_CONTROLLER = new EventController(); 
    public static NotificationsController NOTIFICATIONS_CONTROLLER = new NotificationsController();
    EventsService eventsService = null;
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            // start the logger
            FileHandler fileTxt = new FileHandler("./Eventservicelog.%u.%g.txt",1024*1024,5);
            FileHandler cqlFile = new FileHandler("./cqllog.%u.txt");
            fileTxt.setFormatter(new SimpleFormatter());
            cqlFile.setFormatter(new SimpleFormatter());
            // remove the console logger from the root logger
            Logger rootLogger = LOGGER.getLogger("");
            Handler[] handlers = rootLogger.getHandlers();
            if (handlers[0] instanceof ConsoleHandler) {
                rootLogger.removeHandler(handlers[0]);
            }
            LOGGER.setUseParentHandlers(false);
            LOGGER.addHandler(fileTxt);
            CQL_LOGGER.setUseParentHandlers(false);
            CQL_LOGGER.addHandler(cqlFile);
//            CassandraConstants.CQL_LOG = CQL_LOGGER;
            
            // get the network port number from the command line parameter
            if(args.length == 0){
                System.out.println("Usage: java EventsService.class port");
                System.exit(1);
            }
            try{
                if((PORT = Integer.parseInt(args[0])) < 1024){
                    System.out.println("Port must be > 1024.");
                    System.exit(1);
                }
            } catch(NumberFormatException e) {
                System.out.println("Port must be an integer.");
                System.exit(1);
            }
            
            // get IP address of local host
            IP_ADDRESS = InetAddress.getLocalHost().getHostAddress();
            
            startup();
            manageService();
            shutdown();
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
            System.exit(1);
        }
    }
    
    /**
     * Manage the service thread.
     */
    private static void manageService(){
        //Query Check
        while(running) {
            try {
                //Update required services
                if(running){
                    for(int i = 0; i < 300; i++){
                        if(!running) break;
                        Thread.sleep(1000);
                    }
                    
                    if (names != null) {
                        for(String name : names) {
                            try {
                                mutex.acquire();
                                if (!query(name,LOGGER)) {
                                    System.out.println("Query failed for " + name);
                                    LOGGER.log(Level.INFO, "Query failed for {0}", name);
                                }
                            }  finally {
                                mutex.release();
                            }
                        }
                    }
                }
            }catch (Exception e) {
                processLocalEvent(SERVICE_NAME, e.getStackTrace());
                LOGGER.log(Level.SEVERE, e.getMessage(), e);
            } 
        }
    }
    
    /**
     * Startup the service, start requestsListener, and registering the service
     * @throws Exception 
     */
    private static void startup() throws Exception{
        System.out.println("Startup initiated...");
        LOGGER.log(Level.INFO, "Startup initiated...");
        boolean success;
        
        CLIENT = new EmailClient();
        
        // Starting Listener
        listener = new RequestListener(PORT);
        listener.start();
        
        try{
            mutex.acquire();
            
            // Registering
            System.out.println("Registering service instance...");
            LOGGER.log(Level.INFO, "Registering service instance...");
            do{
                if (!(success = register(SERVICE_NAME, IP_ADDRESS, PORT, LOGGER))) {
                    Thread.sleep(2000L);
                    System.out.println("Retry Registering service...");
                }
            }while(success == false);
            System.out.println("Service instance registered successfully.");
            LOGGER.log(Level.INFO, "Service instance registered successfully.");
            
            // Subscribing
            if (names != null) {
                System.out.println("Subscribing...");
                LOGGER.log(Level.INFO, "Subscribing...");
                do{
                    if (!(success = subscribe(names, IP_ADDRESS, PORT, "", "service", LOGGER))){
                        Thread.sleep(2000L);
                        System.out.println("Retry Subscribing...");
                    }
                }while(success == false);
                System.out.println("Subscribing successfully.");
                LOGGER.log(Level.INFO, "Subscribing successfully.");
            }
        } catch(Exception e){
            LOGGER.log(Level.SEVERE, "Startup failed.");
            LOGGER.log(Level.SEVERE, e.getMessage(),e);
        } finally {
            mutex.release();
        }
    }
    
    /**
     * Shuts down the service cleanly by unregistering the service, stopping all threads, and closing the Cassandra cluster connection.
     */
    private static void shutdown(){
        System.out.println("Shut down initiated...");
        LOGGER.log(Level.INFO, "Shut down initiated...");
        boolean success;
        
        try{
            // Unsubscribing
            if (names != null) {
                System.out.println("Unsubscribing...");
                LOGGER.log(Level.INFO, "Unsubscribing...");
                do{
                    
                    if (!(success = unsubscribe(names, IP_ADDRESS, PORT, "", "service", LOGGER))){
                        Thread.sleep(2000L);
                        System.out.println("Retry Unsubscribing...");
                    }
                }while(success == false);
                System.out.println("Unsubscribing successfully.");
                LOGGER.log(Level.INFO, "Unsubscribing successfully.");
            }
            
            // Unregistering
            System.out.println("Unregistering service instance...");
            LOGGER.log(Level.INFO, "Unregistering service instance...");
            do{
                if (!(success = unregister(SERVICE_NAME, IP_ADDRESS, PORT, LOGGER))){
                    Thread.sleep(2000L);
                    System.out.println("Retry Unregistering service...");
                }
            }while(success == false);
            System.out.println("Service instance unregistered successfully.");
            LOGGER.log(Level.INFO, "Service instance unregistered successfully.");
        } catch(Exception e){
            processLocalEvent(SERVICE_NAME, e.getStackTrace());
            LOGGER.log(Level.SEVERE, e.getMessage(),e);
        }
        
        listener.stop();
        CassandraData.close();
        LOGGER.log(Level.INFO, "Exiting.");
        System.exit(0);
    }
    
    /**
     * Persist Event
     * Check if Notification needs to be sent
     * Create and Send json to Email service
     * @param application, String of the application that occurred
     * @param stackTrace, Array of StackTraceElement objects
     */
    public static void processLocalEvent(String application, StackTraceElement[] stackTrace){
        StringBuilder data = new StringBuilder();
        List<String> emails = new ArrayList();
        List<EventsApplication> nvoList;
        StringBuilder sb;
        Events evo;
        
        try {
            LOGGER.log(Level.INFO, "EventsService.processLocalEvent(): Processing received event.");
            for(StackTraceElement ele : stackTrace){
                data.append(ele.toString()).append("<br />");
            }
            
            // set EventVO
            evo = new Events();
            evo.setData(data.toString());
            evo.setApplication(SERVICE_NAME);
            evo.setIpAddress(IP_ADDRESS);
            evo.setPort(String.valueOf(PORT));
            evo.setCreatedDate(Uuids.timeBased());
            
            // Persist Event
            EVENT_CONTROLLER.createEventController(evo);
            
            // Check if Notification needs to be sent
            if(!(nvoList = NOTIFICATIONS_CONTROLLER.findNotifications(evo.getApplication())).isEmpty()){
                nvoList.forEach(nvo -> {
//                    if(nvo.getEventNotification() == 1 && !emails.stream().anyMatch(email -> email.equalsIgnoreCase(nvo.getEventEmail())))
//                        emails.add(nvo.getEventEmail());
//                    if(nvo.getAitNotification() == 1 && !emails.stream().anyMatch(email -> email.equalsIgnoreCase(nvo.getAitEmail()))) 
//                        emails.add(nvo.getAitEmail());
                      if(nvo.getNotification())
                          emails.add(nvo.getEmail());
                });
            }
            
            // Create and send json to Email service
            if(!emails.isEmpty()) {
                sb = new StringBuilder();
                sb.append("{\"to_emails\":[");
                for(int i = 0; i < emails.size(); i++){
                    sb.append("{\"to_email\":\"").append(emails.get(i)).append((i < emails.size() - 1) ? "\"}," : "\"}");  
                }
                sb
                    .append("],\"from_email\":\"DoNotReply@uptime-solutions.us\",")
                    .append("\"subject\":\"Event Notification for ").append(nvoList.get(0).getApplication()).append("\",")
                    .append("\"body\":\"").append(evo.getData()).append("\"")
                    .append("}");

                CLIENT.sendEmail(sb.toString());
            }
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
    }
    
    /**
     * Sends an email via the Email service
     * @param content the content to add to the body of the email
     */
     @Override
    public void sendEmail(String content){
        // TODO: construct a JSON to post to the Email service. Include IP address of this service instance
        ServiceHostVO current; 
        String service = "Email";
        boolean emailing = false;
        int attempts, count = 0;
        
        List<String> emails = new ArrayList();
        List<EventsApplication> nvoList;
        StringBuilder sb = new StringBuilder();
        
        EventsApplicationDAO eventAppDao = EventsMapperImpl.getInstance().eventsApplicationDAO();
        // Check if Notification needs to be sent
        if (!(nvoList = eventAppDao.findByApplication(SERVICE_NAME)).isEmpty()) {
            emailing = true;
            nvoList.forEach(nvo -> {
                if (nvo.getNotification()) {
                    emails.add(nvo.getEmail());
                }
            });
        }
        // Create json to send to Email service
        if (emailing && !emails.isEmpty()) {
            sb.append("{\"to_emails\":[");
            for (int i = 0; i < emails.size(); i++) {
                sb.append("{\"to_email\":\"").append(emails.get(i)).append((i < emails.size() - 1) ? "\"}," : "\"}");
            }
            sb
                .append("],\"from_email\":\"DoNotReply@uptime-solutions.us\",")
                .append("\"subject\":\"Exception occured in ").append(SERVICE_NAME).append(" Service on ").append(IP_ADDRESS).append(":").append(PORT).append("\",")
                .append("\"body\":\"").append(content).append("\"")
                .append("}");
        }
        try {
            while (emailing && count < 2) {

                // Query Email service if needed
                try {
                    mutex.acquire();
                    if(!getServiceHosts().containsKey(service) || getServiceHosts().get(service).isEmpty())
                        query(service, LOGGER);
                }catch (Exception e) {
                    LOGGER.log(Level.SEVERE, e.getMessage(), e);
                } finally {
                    mutex.release();
                }

                // Attempt to send email
                if(!getServiceHosts().get(service).isEmpty()) {
                    attempts = 0;
                    do {
                        try {
                            current = getServiceHosts().get(service).get((int)EMAIL_INDEX.get());
                            try {
                                send(current, sb.toString()); // Sending Email
                                attempts = 5;
                                emailing = false;
                            } catch (Exception e) {
                                LOGGER.log(Level.WARNING, e.getMessage(), e);

                                // Publishing CircuitBreaker
                                try{
                                    mutex.acquire();
                                    publishCircuitBreaker (current, service, LOGGER);
                                }catch (Exception ex) {
                                    LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
                                } finally {
                                    mutex.release();
                                }
                                ++attempts;
                            }
                            EMAIL_INDEX.incrementAndGet();
                        } catch (IndexOutOfBoundsException e) {
                            if (EMAIL_INDEX.get() == 0L)
                                break;
                            else 
                                EMAIL_INDEX.set(0L);
                        }
                    } while(attempts < 5);
                }
                ++count;
            }
        } catch (Exception e) {
            LOGGER.log(Level.WARNING, e.getMessage(), e);
        }
    }
    
    /**
     * POST http command to send an email through the Email Service
     * @param host, ServiceHostVO object
     * @param json, String object
     * @throws Exception 
     */
    private void send(ServiceHostVO host, String json) throws Exception {
        HttpURLConnection connection;
        int code;

        connection = (HttpURLConnection) new URL("http://" + host.getIp() + ":" + host.getPort() + "/email").openConnection();
        connection.setReadTimeout(READ_TIMEOUT);
        connection.setConnectTimeout(CONNECTION_TIMEOUT);
        connection.setRequestMethod("POST");
        connection.setDoInput(true);
        connection.setDoOutput(true);

        // Post Json
        try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(connection.getOutputStream(), "UTF-8"))) {
            writer.write(json);
        }

        // Check response Status code
        if ((code = connection.getResponseCode()) != 200) {
            throw new Exception("Error, Status Code:" + code); 
        }
    }
}
