/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.events.http.client;

import static com.uptime.events.EventsService.LOGGER;
import static com.uptime.events.EventsService.mutex;
import static com.uptime.services.AbstractService.getServiceHosts;
import static com.uptime.services.AbstractService.publishCircuitBreaker;
import static com.uptime.services.AbstractService.query;
import com.uptime.services.ServiceConstants;
import com.uptime.services.vo.ServiceHostVO;
import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.logging.Level;
import java.util.concurrent.atomic.AtomicLong;

/**
 *
 * @author ksimmons
 */
public class EmailClient {
    private final AtomicLong INDEX;
    private final int READ_TIMEOUT;
    private final int CONNECTION_TIMEOUT;
    
    /**
     * Constructor
     */
    public EmailClient() {
        INDEX = new AtomicLong(0L);
        READ_TIMEOUT = 5000;
        CONNECTION_TIMEOUT = 5000;
    }
    
    /**
     * Attempt to send an email through the email service
     * @param json, String object to be sent to the email service 
     */
    public void sendEmail (String json){
        if(!ServiceConstants.DEVELOPING_TESTING) {
            ServiceHostVO current; 
            String service = "Email";
            boolean emailing = true;
            int attempts, count = 0;

            try {
                while (emailing && count < 2) {

                    // Query Email service if needed
                    try {
                        mutex.acquire();
                        if(!getServiceHosts().containsKey(service) || getServiceHosts().get(service).isEmpty())
                            query(service,LOGGER);
                    }catch (Exception e) {
                        LOGGER.log(Level.SEVERE, e.getMessage(), e);
                    } finally {
                        mutex.release();
                    }

                    // Attempt to send email
                    if(!getServiceHosts().get(service).isEmpty()) {
                        attempts = 0;
                        do {
                            try {
                                current = getServiceHosts().get(service).get((int)INDEX.get());
                                try {
                                    send(current, json); // Sending Email
                                    attempts = 5;
                                    emailing = false;
                                } catch (Exception e) {
                                    LOGGER.log(Level.WARNING, e.getMessage(), e);

                                    // Publishing CircuitBreaker
                                    try{
                                        mutex.acquire();
                                        publishCircuitBreaker (current, service, LOGGER);
                                    }catch (Exception ex) {
                                        LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
                                    } finally {
                                        mutex.release();
                                    }
                                    ++attempts;
                                }
                                INDEX.incrementAndGet();
                            } catch (IndexOutOfBoundsException e) {
                                if (INDEX.get() == 0L)
                                    break;
                                else 
                                    INDEX.set(0L);
                            }
                        } while(attempts < 5);
                    }
                    ++count;
                }
            } catch (Exception e) {
                LOGGER.log(Level.WARNING, e.getMessage(), e);
            }
        }
    }
    
    /**
     * POST http command to send an email through the Email Service
     * @param host, ServiceHostVO object
     * @param json, String object
     * @throws Exception 
     */
    private void send(ServiceHostVO host, String json) throws Exception {
        HttpURLConnection connection;
        int code;

        connection = (HttpURLConnection) new URL("http://" + host.getIp() + ":" + host.getPort() + "/email").openConnection();
        connection.setReadTimeout(READ_TIMEOUT);
        connection.setConnectTimeout(CONNECTION_TIMEOUT);
        connection.setRequestMethod("POST");
        connection.setDoInput(true);
        connection.setDoOutput(true);
        
        // Post Json
        try(BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(connection.getOutputStream(), "UTF-8"))){
            writer.write(json);
        }
        
        // Check response Status code
        if ((code = connection.getResponseCode()) != 200)
            throw new Exception("Error, Status Code:" + code);
    }
}
