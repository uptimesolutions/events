/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.events.http.handlers;

import com.datastax.oss.driver.api.core.uuid.Uuids;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.uptime.cassandra.events.entity.Events;
import com.uptime.cassandra.events.entity.EventsApplication;
import static com.uptime.events.EventsService.CLIENT;
import static com.uptime.events.EventsService.LOGGER;
import static com.uptime.events.EventsService.SERVICE_NAME;
import static com.uptime.events.EventsService.EVENT_CONTROLLER;
import static com.uptime.events.EventsService.NOTIFICATIONS_CONTROLLER;
import static com.uptime.events.EventsService.processLocalEvent;
import static com.uptime.events.EventsService.running;
import com.uptime.services.http.handler.AbstractGenericHandler;
import static com.uptime.services.util.HttpUtils.parseQuery;
import static com.uptime.services.util.ServiceUtil.streamReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Logger;

/**
 *
 * @author ksimmons
 */
public class EventsHandler extends AbstractGenericHandler {
    
    /**
     * HTTP GET handler
     * @param he
     * @throws IOException
     */
    @Override
    public void doGet(HttpExchange he) throws IOException {
        StackTraceElement[] stackTrace = null;
        Map<String, Object> params;
        byte[] response;
        OutputStream os;
        Headers respHeaders = he.getResponseHeaders();
        respHeaders.set("Content-Type", "application/json");
        String tmp;

        try {
            params = parseQuery(he.getRequestURI().getQuery());
            if (params.isEmpty()) {
                response = "{\"outcome\":\"Hello World!\"}".getBytes();
                he.sendResponseHeaders(HttpURLConnection.HTTP_OK, response.length);
            } else if (params.containsKey("application")) {
                tmp = eventListToJson(EVENT_CONTROLLER.getEventsByApplication(params.get("application").toString().toUpperCase()));
                response = tmp.getBytes();
                he.sendResponseHeaders(tmp.contains("Does not Exist") ? HttpURLConnection.HTTP_NOT_FOUND : HttpURLConnection.HTTP_OK, response.length);
            } else {
                response = "{\"outcome\":\"Unknown Params\"}".getBytes();
                he.sendResponseHeaders(HttpURLConnection.HTTP_NOT_ACCEPTABLE, response.length);
            }
        } catch (NumberFormatException e) {
            stackTrace = e.getStackTrace();
            response = "{\"outcome\":\"NumberFormatException\"}".getBytes();
            he.sendResponseHeaders(HttpURLConnection.HTTP_BAD_METHOD, response.length);
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        } catch (UnsupportedEncodingException e) {
            stackTrace = e.getStackTrace();
            response = "{\"outcome\":\"UnsupportedEncodingException\"}".getBytes();
            he.sendResponseHeaders(HttpURLConnection.HTTP_BAD_METHOD, response.length);
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        } catch (Exception e) {
            stackTrace = e.getStackTrace();
            response = "{\"outcome\":\"Exception\"}".getBytes();
            he.sendResponseHeaders(HttpURLConnection.HTTP_BAD_METHOD, response.length);
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }

        os = he.getResponseBody();
        os.write(response);
        os.flush();
        he.close();

        // Send Event
        if (stackTrace != null) {
            processLocalEvent(SERVICE_NAME, stackTrace);
        }
    }
    
    /**
     * HTTP POST handler
     * @param he
     * @throws IOException
     */
    @Override
    public void doPost(HttpExchange he) throws IOException {
        StackTraceElement[] stackTrace = null;
        JsonElement element;
        JsonObject jsons;
        Events ev = null;
        boolean good = false;
        byte[] response = null;

        try {
            if (he.getRequestBody() == null) {
                response = "{\"outcome\":\"Unknown Params\"}".getBytes();
                he.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, response.length);
            } else {
                element = JsonParser.parseString(streamReader(he.getRequestBody()));
                jsons = element.getAsJsonObject();
                JsonObject json = jsons.getAsJsonObject();

                LOGGER.log(Level.INFO, "EventsHandler.doPost(): JSON={0}", json);
                if (json != null) {
                    if (json.has("application") && json.has("created_date") && json.has("data") && json.has("ip_address") && json.has("port")) {
                        UUID CreatedDate = Uuids.startOf(json.get("created_date").getAsLong());
                        ev = new Events();
                        ev.setApplication(json.get("application").getAsString().toUpperCase());
                        ev.setCreatedDate(UUID.fromString(CreatedDate.toString().replaceAll("\"", "")));
                        ev.setData(json.get("data").getAsString());
                        ev.setIpAddress(json.get("ip_address").getAsString());
                        ev.setPort(json.get("port").getAsString());
                        String resp = EVENT_CONTROLLER.createEventController(ev);
                        LOGGER.log(Level.INFO, "createEvent resp : {0}", resp);
                        response = resp.getBytes();
                        good = true;
                        if (resp.contains("success")) {
                            he.sendResponseHeaders(HttpURLConnection.HTTP_OK, response.length);
                        } else if (resp.contains("Error")) {
                            he.sendResponseHeaders(HttpURLConnection.HTTP_UNAVAILABLE, response.length);
                        } else {
                            he.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, response.length);
                        }
                    } else {
                        response = "{\"outcome\":\"Event Json is invalid\"}".getBytes();
                        he.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, 0);
                    }
                } else {
                    response = "{\"outcome\":\"Event Json cannot be blank\"}".getBytes();
                    he.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, response.length);
                }
            }
        } catch (Exception e) {
            stackTrace = e.getStackTrace();
            he.sendResponseHeaders(HttpURLConnection.HTTP_BAD_METHOD, 0);
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
        Headers respHeaders = he.getResponseHeaders();
        respHeaders.set("Content-Type", "application/json");
        OutputStream os = he.getResponseBody();
        os.write(response);
        os.flush();
        he.close();

        // Send Event
        if (stackTrace != null) {
            processLocalEvent(SERVICE_NAME, stackTrace);
        } else if (good && ev != null) {
            processEvent(ev);
        }
    }
    
    /**
     * Persist Event Check if Notification needs to be sent Create and Send json
     * to Email service
     *
     * @param evo, EventVO object
     */
    private void processEvent(Events evo) {
        List<String> emails = new ArrayList();
        List<EventsApplication> nvoList;
        StringBuilder sb;

        try {
            // Persist Event
//          EVENT_CONTROLLER.createEvent(evo);

            // Check if Notification needs to be sent
            if (!(nvoList = NOTIFICATIONS_CONTROLLER.findNotifications(evo.getApplication())).isEmpty()) {
                nvoList.forEach(nvo -> {
//                    if(nvo.getEventNotification() == 1 && !emails.stream().anyMatch(email -> email.equalsIgnoreCase(nvo.getEventEmail())))
//                        emails.add(nvo.getEventEmail());
//                    if(nvo.getAitNotification() == 1 && !emails.stream().anyMatch(email -> email.equalsIgnoreCase(nvo.getAitEmail()))) 
//                        emails.add(nvo.getAitEmail());
                    if (nvo.getNotification()) {
                        emails.add(nvo.getEmail());
                    }
                });
            }
            // Create and send json to Email service
            if (!emails.isEmpty()) {
                sb = new StringBuilder();
                sb.append("{\"to_emails\":[");
                for (int i = 0; i < emails.size(); i++) {
                    sb.append("{\"to_email\":\"").append(emails.get(i)).append((i < emails.size() - 1) ? "\"}," : "\"}");
                }
                sb
                        .append("],\"from_email\":\"DoNotReply@upcastcm.com\",")
                        .append("\"subject\":\"Event Notification for ").append(nvoList.get(0).getApplication())
                        .append(" ").append(evo.getIpAddress() != null ? evo.getIpAddress(): "").append("\",")
                        .append("\"body\":\"").append(evo.getData()).append("\"")
                        .append("}");
                CLIENT.sendEmail(sb.toString());
            }
        } catch (Exception e) {
            processLocalEvent(SERVICE_NAME, e.getStackTrace());
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
    }
    
    /**
     * Convert a list of EventVO to a json
     * @param list of EventVO objects
     * @return String, json
     */
    public String eventListToJson(List<Events> list) {
        StringBuilder sb = new StringBuilder();

        if (list.isEmpty()) {
            return "{\"outcome\":\"Does not Exist\"}";
        } else {
            sb.append("{\"events\":[");
            for (int i = 0; i < list.size(); i++) {
                Long longTimstamp = Uuids.unixTimestamp(list.get(i).getCreatedDate());
                java.util.Date time = new java.util.Date((long) longTimstamp);
                sb
                        .append("{\"application\":\"").append(list.get(i).getApplication()).append("\"")
                        .append(",\"created_date\":\"").append(time).append("\"")
                        .append(",\"data\":\"").append(list.get(i).getData()).append("\"")
                        .append(",\"ip_address\":\"").append(list.get(i).getIpAddress()).append("\"")
                        .append(",\"port\":\"").append(list.get(i).getPort()).append("\"");
                sb.append("}").append(i < list.size() - 1 ? "," : "");
            }
            sb.append("]}");
            return sb.toString();
        }
    }
    
    @Override
    public boolean getRunning() {
        return running;
    }

    @Override
    public Logger getLogger() {
        return LOGGER;
    }
}
