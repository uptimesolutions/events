/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.events.http.handlers;

import com.uptime.services.http.handler.AbstractSystemHandler;
import static com.uptime.events.EventsService.IP_ADDRESS;
import static com.uptime.events.EventsService.LOGGER;
import static com.uptime.events.EventsService.PORT;
import static com.uptime.events.EventsService.SERVICE_NAME;
import static com.uptime.events.EventsService.mutex;
import static com.uptime.events.EventsService.names;
import static com.uptime.events.EventsService.processLocalEvent;
import static com.uptime.events.EventsService.running;
import java.util.concurrent.Semaphore;
import java.util.logging.Logger;

/**
 *
 * @author ksimmons
 */
public class SystemHandler extends AbstractSystemHandler {
    @Override
    public boolean getRunning() {
        return running;
    }

    @Override
    public void setRunning(boolean value) {
        running = value;
    }

    @Override
    public Logger getLogger() {
        return LOGGER;
    }

    @Override
    public String getServiceName() {
        return SERVICE_NAME;
    }

    @Override
    public int getServicePort() {
        return PORT;
    }

    @Override
    public String getServiceIpAddress() {
        return IP_ADDRESS;
    }

    @Override
    public String[] getServiceSubscribeNames() {
        return names;
    }

    @Override
    public Semaphore getMutex() {
        return mutex;
    }

    @Override
    public void sendEvent(StackTraceElement[] stackTrace) {
        processLocalEvent(SERVICE_NAME, stackTrace);
    }
}
