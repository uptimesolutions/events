/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.events.http.listeners;

import com.sun.net.httpserver.HttpServer;
import static com.uptime.events.EventsService.LOGGER;
import com.uptime.events.http.handlers.EventsHandler;
import com.uptime.events.http.handlers.SystemHandler;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.concurrent.Executors;
import java.util.logging.Level;

/**
 *
 * @author ksimmons
 */
public class RequestListener {
    private int port = 0;
    HttpServer server;
    
    public RequestListener(int port){
        this.port = port;
    }
    
    public void stop(){
        LOGGER.log(Level.INFO, "Stopping request listener...");
        server.stop(10);
    }
    
    public void start() throws IOException{
        server = HttpServer.create(new InetSocketAddress(port), 2);
        server.createContext("/system", new SystemHandler());
        server.createContext("/events", new EventsHandler());
        server.setExecutor(Executors.newCachedThreadPool());
        
        //start server
        LOGGER.log(Level.INFO, "Starting request listener...");
        server.start();

    }
}
