/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.events.controller;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.datastax.oss.driver.api.core.servererrors.WriteTimeoutException;
import com.uptime.cassandra.CassandraConstants;
import com.uptime.cassandra.events.dao.EventsApplicationDAO;
import com.uptime.cassandra.events.dao.EventsDAO;
import com.uptime.cassandra.events.dao.EventsMapperImpl;
import com.uptime.cassandra.events.entity.Events;
import com.uptime.cassandra.events.entity.EventsApplication;
import java.util.List;
import java.util.logging.Level;
import static com.uptime.events.EventsService.LOGGER;
import static com.uptime.events.EventsService.SERVICE_NAME;
import static com.uptime.events.EventsService.processLocalEvent;
import java.util.logging.Logger;

/**
 *
 * @author ksimmons
 */
public class EventController {

    private final EventsDAO eventDao;
    private final EventsApplicationDAO eventAppDao;
    private Events events;
    private EventsApplication eventsApp;

    /**
     * Constructor
     */
    public EventController() {
//        CassandraConstants.CQL_LOG = Logger.getLogger(EventController.class.getName());
        eventDao = EventsMapperImpl.getInstance().eventsDao();
        eventAppDao = EventsMapperImpl.getInstance().eventsApplicationDAO();
    }

    public String createEventController(Events entityObj) {

        EventController cev = new EventController();
        if (cev.createEvents(entityObj)) {
            LOGGER.log(Level.INFO, " Events created successfully");
            return "{\"outcome\":\"Events created successfully\"}";
        } else {
            LOGGER.log(Level.INFO, "Error occured when creating Events");
            return "{\"outcome\":\"Error occured when creating Events\"}";
        }
    }

    public boolean createEvents(Events entityObj) {
        events = null;

        if (entityObj.getApplication() != null && entityObj.getApplication().length() > 0) {
            setEvents(entityObj);
        }

        // insert rows
        try {
            // set TTL to 72 hours
            if (events != null) {
                eventDao.create(events, 259200);
            }
            LOGGER.log(Level.INFO, "Set Events");
            return true;
        } catch (UnavailableException | WriteTimeoutException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
            processLocalEvent(SERVICE_NAME, e.getStackTrace());
        } catch (IllegalArgumentException e) {
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
            processLocalEvent(SERVICE_NAME, e.getStackTrace());
        }

        return false;
    }

    private void setEvents(Events entityObj) {
        events = new Events();
        events.setApplication(entityObj.getApplication());
        events.setCreatedDate(entityObj.getCreatedDate());
        events.setData(entityObj.getData());
        events.setIpAddress(entityObj.getIpAddress());
        events.setPort(entityObj.getPort());
    }

    public List<Events> getEventsByApplication(String application) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return eventDao.findByApplication(application);
    }
    
    public String createEventApplicationController(EventsApplication entityObj) {

        EventController cev = new EventController();
        if (cev.createEventsApplication(entityObj)) {
            LOGGER.log(Level.INFO, " EventsApplication created successfully");
            return "{\"outcome\":\"EventsApplication created successfully\"}";
        } else {
            LOGGER.log(Level.INFO, "Error occured when creating EventsApplication");
            return "{\"outcome\":\"Error occured when creating EventsApplication\"}";
        }
    }

    public boolean createEventsApplication(EventsApplication entityObj) {
        events = null;

        if (entityObj.getApplication() != null && entityObj.getApplication().length() > 0) {
            setEventsApplication(entityObj);
        }

        // insert rows
        try {
            if (eventsApp != null) {
                eventAppDao.create(eventsApp);
            }
            LOGGER.log(Level.INFO, "Created EventsApplication");
            return true;
        } catch (UnavailableException | WriteTimeoutException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
            processLocalEvent(SERVICE_NAME, e.getStackTrace());
        } catch (IllegalArgumentException e) {
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
            processLocalEvent(SERVICE_NAME, e.getStackTrace());
        }

        return false;
    }

    private void setEventsApplication(EventsApplication entityObj) {
        eventsApp = new EventsApplication();
        eventsApp.setApplication(entityObj.getApplication());
        eventsApp.setEmail(entityObj.getEmail());
        eventsApp.setNotification(entityObj.getNotification());
    }
      
    public List<EventsApplication> getEventsApplicationByApplication(String application) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return eventAppDao.findByApplication(application);
    }
}   
