/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.events.controller;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.events.dao.EventsApplicationDAO;
import com.uptime.cassandra.events.dao.EventsMapperImpl;
import com.uptime.cassandra.events.entity.EventsApplication;
import java.util.List;

/**
 *
 * @author ksimmons
 */
public class NotificationsController {
    private final EventsApplicationDAO eventAppDao;
    //private EventApplication eventApp;

    /**
     * Constructor
     */
    public NotificationsController() {
        eventAppDao = EventsMapperImpl.getInstance().eventsApplicationDAO();
    }
    
    /**
     * returns a list of EventApplication objects from the EventApplicationADO
     * @param application, String
     * @return list of NotificationsView objects
     * @throws NoHostAvailableException, UnavailableException, ReadTimeoutException, IllegalArgumentException
     */
    public List<EventsApplication> findNotifications (String application) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return eventAppDao.findByApplication(application);
    }
}
