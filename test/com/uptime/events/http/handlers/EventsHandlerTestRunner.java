/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.events.http.handlers;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;
/**
 *
 * @author madha
 */
public class EventsHandlerTestRunner {
    public static void main(String[] args) {
      Result result = JUnitCore.runClasses(EventsHandlerTester.class);
      for (Failure failure : result.getFailures()) {
         System.out.println("fail - "+failure.toString());
      }
      System.out.println("passed:"+result.wasSuccessful());
   }
}
