/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.events.http.handlers;

import java.io.IOException;
import java.io.StringWriter;
import org.apache.commons.io.IOUtils;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

/**
 *
 * @author madha
 */
public class TestEvents {
    
    public void testDoGet() throws Exception {
        System.out.println("doGet");

          StringWriter writer;
          CloseableHttpResponse closeableHttpResponse = getMethod("http://localhost:8082/events?application=event_service");
          IOUtils.copy(closeableHttpResponse.getEntity().getContent(), (writer = new StringWriter()));
          System.out.println("doGet response - " + writer.toString());
    }
    
    
    // create GET Method, which will call the URL and get the response in the form of JSON object without Header
    public CloseableHttpResponse getMethod(String Url) throws ClientProtocolException, IOException{
         
        /*Call HTTPClients class from HTTPClient library added in POM.xml.
        Call createDefault() method present in HTTPClients class, which will create a client connection.
        And this createDefault() method returns "CloseableHttpClient" object which is an abstract class. 
        And we are creating a reference to that abstract class */

        CloseableHttpClient httpClient=HttpClients.createDefault();

        /*create an object for HttpGet class, which is used for HTTP GET Request. And pass the URL which is to be loaded*/
        HttpGet htttpGet = new HttpGet(Url); 

        /*execute the HTTP GET Request, means it will hit the GET API call as we click SEND button from POSTMAN client. 
        httpClient.execute() method returns the response "CloseableHttpResponse" interface and store it in reference variable
        So the complete response is stored in CloseableHttpResponse
        And fetch all the details, in our test case/test method */

        CloseableHttpResponse closeableHttpResponse = httpClient.execute(htttpGet); 
        return closeableHttpResponse; 
    }
    
    
    public static void main(String[] args){
        TestEvents te = new TestEvents();
        try {
            te.testDoGet();
        }
        catch (Exception e) {
            System.out.println("Error Occured" + e.toString());
        }
    }
}
