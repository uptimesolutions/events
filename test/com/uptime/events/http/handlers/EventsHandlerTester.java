/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.events.http.handlers;

import com.uptime.cassandra.events.entity.Events;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.apache.cassandra.utils.UUIDGen;
import org.junit.Test;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;

/**
 *
 * @author madha
 */
public class EventsHandlerTester {
    
    
    @BeforeAll
    public static void setUpClass() {
        
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {

    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of doGet method, of class EventsHandler.
     */
    @Test
    public void testEventListToJson() throws Exception {
        List<Events> eventList;

        eventList = new ArrayList<>();
        Events event = new Events();
        event.setApplication("Maintenance");
        event.setCreatedDate(UUIDGen.getTimeUUID());
        event.setData("Test Exception Message 2222");
        event.setIpAddress("10.1.1.1");
        event.setPort("8090");
        eventList.add(event);
        event = new Events();
        event.setApplication("Maintenance");
        event.setCreatedDate(UUIDGen.getTimeUUID());
        event.setData("Test Exception Message 4444");
        event.setIpAddress("10.1.1.2");
        event.setPort("8091");
        eventList.add(event);
        System.out.println("eventList " + eventList.size());
        EventsHandler instance = new EventsHandler();
        String json = instance.eventListToJson(eventList);
        System.out.println("json -  " + json);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
         
    }
    
}
