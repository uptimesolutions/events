/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.events.controller;

import com.uptime.cassandra.events.entity.Events;
import com.uptime.cassandra.events.entity.EventsApplication;
import java.util.List;
import java.util.UUID;
import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import static org.junit.Assert.*;

/**
 *
 * @author aswani
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class EventControllerTest {
    
    @Rule
    public CassandraCQLUnit cassandraCQLUnit =
            new CassandraCQLUnit(new ClassPathCQLDataSet("resources//events.cql", true, true,"worldview_dev1"));

    @Test
    public void test40_ControllerGetEvents() throws Exception {
        System.out.println("start test41_ControllerGetEvents");
        EventController cev = new EventController();
        List<Events> eventsList = cev.getEventsByApplication("Maintenance");
        if (eventsList != null && !eventsList.isEmpty()) {
            System.out.println("eventsList size - " + eventsList.size());
            for (int i = 0, size = eventsList.size(); i < size; i++) {
                System.out.println("events- " + eventsList.get(i).getApplication() + " - " + eventsList.get(i).getData());
            }
            assertThat(eventsList.size(), is(1));
        }
        System.out.println("end test41_ControllerGetEvents");
    }
    @Test(expected = IllegalArgumentException.class)
    public void test41_ControllerGetEvents() throws Exception {
        System.out.println("start test41_ControllerGetEvents");
        EventController cev = new EventController();
        fail(cev.getEventsByApplication(null).toString());
        System.out.println("end test41_ControllerGetEvents");
    }
        
    @Test
    public void test30_ControllerGetEventsApplication() throws Exception {
        System.out.println("start test30_ControllerGetEventsApplication");
        EventController cev = new EventController();
        List<EventsApplication> eventsList = cev.getEventsApplicationByApplication("Maintenance");
        if (eventsList != null && !eventsList.isEmpty()) {
            System.out.println("eventsApplicationList size - " + eventsList.size());
            for (int i = 0, size = eventsList.size(); i < size; i++) {
                System.out.println("events- " + eventsList.get(i).getApplication() + " - " + eventsList.get(i).getEmail());
            }
            assertThat(eventsList.size(), is(1));
        }
        System.out.println("end test30_ControllerGetEventsApplication");
    }
    @Test(expected = IllegalArgumentException.class)
    public void test31_ControllerGetEventsApplication() throws Exception {
        System.out.println("start test31_ControllerGetEventsApplication");
        EventController cev = new EventController();
        fail(cev.getEventsApplicationByApplication(null).toString());
        System.out.println("end test31_ControllerGetEventsApplication");
    }
    
    @Test
    public void test10_ControllerCreateEvents() throws Exception {
        System.out.println("start test10_ControllerCreateEvents");
        Events event = new Events();
        event.setApplication("Maintenance");
        event.setCreatedDate(UUID.randomUUID());
        event.setData("Test Exception Message 4444");
        event.setIpAddress("10.1.1.2");
        event.setPort("8090");
        EventController cev = new EventController();
        boolean resp;
        if (resp = cev.createEvents(event)) {
            System.out.println("events Created");
            assertThat(resp, is(true));
        }
        System.out.println("end test10_ControllerCreateEvents");
    }
    @Test(expected = AssertionError.class)
    public void test11_ControllerCreateEvents() throws Exception {
        System.out.println("start test11_ControllerCreateEvents");
        Events event = new Events();
        event.setApplication("Maintenance");
//        event.setCreatedDate(UUID.randomUUID());
        event.setData("Test Exception Message 4444");
        event.setIpAddress("10.1.1.2");
        event.setPort("8090");
        EventController cev = new EventController();
        fail(cev.createEvents(event) + " Events creation fail.");
        System.out.println("end test11_ControllerCreateEvents");
    }
    @Test(expected = AssertionError.class)
    public void test12_ControllerCreateEvents() throws Exception {
        System.out.println("start test12_ControllerCreateEvents");
        Events event = new Events();
//        event.setApplication("Maintenance");
//        event.setCreatedDate(UUID.randomUUID());
        event.setData("Test Exception Message 4444");
        event.setIpAddress("10.1.1.2");
        event.setPort("8090");
        EventController cev = new EventController();
        fail(cev.createEvents(event) + " Events creation fail.");
        System.out.println("end test12_ControllerCreateEvents");
    }
    @Test(expected = NullPointerException.class)
    public void test13_ControllerCreateEvents() throws Exception {
        System.out.println("start test13_ControllerCreateEvents");
        Events event = new Events();
//        event.setApplication("Maintenance");
//        event.setCreatedDate(UUID.randomUUID());
        event.setData("Test Exception Message 4444");
        event.setIpAddress("10.1.1.2");
        event.setPort("8090");
        EventController cev = new EventController();
        fail(cev.createEvents(null) + " Events creation fail.");
        System.out.println("end test13_ControllerCreateEvents");
    }
    
    @Test
    public void test20_ControllerCreateEventsApplication() throws Exception {
        System.out.println("start test20_ControllerCreateEventsApplication");
        EventsApplication event = new EventsApplication();
        event.setApplication("Config");
        event.setEmail("madhavi.bathala@maestrodigitalservices.com");
        event.setNotification(true);
        EventController cev = new EventController();
        boolean resp;
        if (resp = cev.createEventsApplication(event)) {
            System.out.println("EventsApplication Created");
            assertThat(resp, is(true));
        }
        System.out.println("end test20_ControllerCreateEventsApplication");
    }
    @Test(expected = NullPointerException.class)
    public void test21_ControllerCreateEventsApplication() throws Exception {
        System.out.println("start test21_ControllerCreateEventsApplication");
        EventController cev = new EventController();
        fail(cev.createEventsApplication(null) + " EventsApplication create failed.");
        System.out.println("end test21_ControllerCreateEventsApplication");
    }
    @Test(expected = AssertionError.class)
    public void test22_ControllerCreateEventsApplication() throws Exception {
        System.out.println("start test22_ControllerCreateEventsApplication");
        EventsApplication event = new EventsApplication();
//        event.setApplication("Config");
        event.setEmail("madhavi.bathala@maestrodigitalservices.com");
        event.setNotification(true);
        EventController cev = new EventController();
        fail(cev.createEventsApplication(event) + " EventsApplication create failed.");
        System.out.println("end test22_ControllerCreateEventsApplication");
    }
    
}
